# Ansible Role c2platform.dev.oracle_database

Create Oracle database for development and test using Docker image [doctorkirk/oracle-19c](https://registry.hub.docker.com/r/doctorkirk/oracle-19c).

- [Requirements](#requirements)
- [Role Variables](#role-variables)
  - [Docker](#docker)
  - [Yum repositories](#yum-repositories)
  - [Wait for](#wait-for)
  - [Restore](#restore)
- [Dependencies](#dependencies)
- [Example](#example)
- [Troubleshooting](#troubleshooting)

## Requirements

<!-- Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required. -->

## Role Variables

<!--  A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well. -->

```yaml
oracle_database_sid: c2d
oracle_database_password: supersecure
oracle_database_port: 1521
```

### Docker

This role includes [c2platform.mw.docker](https://gitlab.com/c2platform/ansible-collection-mw/-/blob/master/roles/docker/README.md) so this provides the lists associated with role prefixed with `oracle_database` e.g.`oracle_database_docker_images`

### Yum repositories

This role includes [c2platform.core.yum](https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/roles/yum/README.md) to configure Oracle RPM's for SQL*Plus.

c2platform.core.yum
    tasks_from: main
  vars:
    yum_repositories: "{{ oracle_database_yum_repositories


### Wait for

Default this role will wait for Oracle to become available using configuration shown below. You can disable this using `oracle_database_wait_for_enabled`.

```yaml
oracle_database_wait_for_enabled: true
oracle_database_wait_for:
  command: echo "select 'Oracle alive' from dual;" | sqlplus -S sys/secret@c2d as sysdba
  retries: 20
  delay: 30
```

### Restore

This role can perform a restore of a dump file using [Oracle Data Pump](https://docs.oracle.com/cd/B19306_01/server.102/b14215/dp_overview.htm) utility `impdp`.

```yaml
oracle_database_restore_enabled: false
oracle_database_restores:
  - directory_name: DUMP_DIR_C10_V3_RESTORE
    directory_path: "{{ oracle_database_dump_dir }}/c10_v3_restore"
    archive: /tmp/dump.tar.gz
    environment:
      ORACLE_HOME: /u01/app/oracle/product/12.2.0/SE
    command: >
      $ORACLE_HOME/bin/impdp
        system/{{ oracle_database_password }}
        schemas=REG_OWNER
        dumpfile=boc%U.dmp
        directory=DUMP_DIR
        logfile=boc.log
```

## Dependencies

<!--   A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles. -->

## Example

<!--   Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too: -->

The node `c2d-oracle1` in the reference project uses this role to create an Oracle database using play [mw/oracle.yml](https://gitlab.com/c2platform/ansible/-/blob/master/plays/mw/oracle.yml) as shown below.

```yaml
---
- name: oracle
  hosts: oracle
  become: yes

  roles:
    - { role: c2platform.core.common, tags: ["common"] }
    - { role: geerlingguy.docker, tags: ["docker"] }
    - { role: c2platform.dev.oracle_database, tags: ["oracle", "database", "docker"] }
    - { role: c2platform.oracle.oracle, tags: ["oracle"] }
```

The configuration for this node is in [group_vars/oracle/main.yml](https://gitlab.com/c2platform/ansible/-/blob/master/group_vars/oracle/main.yml).

## Troubleshooting



The database takes a bit of time to initialize. Default the role will wait for this process to finish to continue. You can monitor this process with

```bash
docker logs -f oracle
```

```
docker exec -it oracle bash
```
