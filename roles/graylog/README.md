# Ansible Role c2platform.dev.graylog

Basic creation of Graylog instance using Docker containers. Based on [Installing GrayLog - Docker](https://go2docs.graylog.org/5-0/downloading_and_installing_graylog/docker_installation.htm).

- [Requirements](#requirements)
- [Role Variables](#role-variables)
  - [Docker](#docker)
  - [REST API](#rest-api)
- [Dependencies](#dependencies)
- [Example](#example)

## Requirements

<!-- Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required. -->

## Role Variables

<!--  A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well. -->

### Docker

This role includes [c2platform.mw.docker](https://gitlab.com/c2platform/ansible-collection-mw/-/blob/master/roles/docker/README.md) so this provides the lists associated with role prefixed with `graylog` e.g.`graylog_docker_images`


### REST API

```yaml
graylog_rest_base_url: "{{ graylog_external_uri }}"
graylog_rest_method: GET
graylog_rest_user: admin
graylog_rest_password: secret
graylog_rest_force_basic_auth: true

graylog_rest_headers:
  Content-Type: application/json

graylog_rest_resources:
  01_authentication_backends:
    resources:
      - id: auth_backend
        url: api/system/authentication/services/backends
```

## Dependencies

<!--   A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles. -->

## Example

<!--   Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too: -->

```yaml
---
- name: graylog
  hosts: graylog
  become: yes

  roles:
    - { role: c2platform.core.common, tags: ["common"] }
    - { role: geerlingguy.docker, tags: ["docker"] }
    - { role: c2platform.dev.graylog, tags: ["monitoring", "graylog"] }
```
