# Ansible Collection - c2platform.dev

[![Pipeline Status](https://gitlab.com/c2platform/ansible-collection-dev/badges/master/pipeline.svg?style=flat-square&key_text=Pipeline+Status&key_width=90)](https://gitlab.com/c2platform/ansible-collection-dev/-/pipelines)
[![Latest Release](https://gitlab.com/c2platform/ansible-collection-dev/-/badges/release.svg?style=flat-square)](https://gitlab.com/c2platform/ansible-collection-dev/-/pipelines)
[![Ansible Galaxy](https://img.shields.io/badge/Galaxy-_c2platform.dev-blue.svg)](https://galaxy.ansible.com/ui/repo/published/c2platform/dev/)

C2 Platform Ansible Collection with development roles. These roles are not intended to be used for production systems!

## Roles

* [`oracle_database`](./roles/oracle_database/).
* [`desktop`](./roles/desktop/).
* [`graylog`](./roles/graylog/).

## Modules and Filters

For detailed information on the available modules and filters within this
collection, please refer to the
[Ansible Galaxy](https://galaxy.ansible.com/ui/repo/published/c2platform/dev/docs/)
page.

You can of course also use the `ansible-doc` command to explore the
documentation:

```bash
ansible-doc -t module --list c2platform.dev
ansible-doc -t filter --list c2platform.dev
ansible-doc -t filter <FILTER_NAME>
ansible-doc -t module <MODULE_NAME>
```
