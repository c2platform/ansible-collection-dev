# CHANGELOG

## 1.0.4 ( 2024-06-26 )

* `.gitlab-ci.yml` based on `c2platform.core`
* [`vagrant_hosts`](./roles/vagrant_hosts/) moved to `c2platform.core`.

## 1.0.3 ( 2024-05-31 )

* [`vagrant_hosts`](./roles/vagrant_hosts/) add to `become: false` to
  `c2platform.dev.vagrant_hosts`.

## 1.0.2 ( 2024-05-06 )

* [`vagrant_hosts`](./roles/vagrant_hosts/) new variable
  `vagrant_hosts_content_win`.

## 1.0.1 ( 2024-04-10 )

* [`vagrant_hosts`](./roles/vagrant_hosts/) new role.
* [`graylog`](./roles/graylog/../../README.md) create graylog instance using docker containers.
* [`desktop`](./roles/desktop/../../README.md) new create / manage Ansible development desktop.
