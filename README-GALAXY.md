# Ansible Collection - c2platform.dev

[![Pipeline Status](https://gitlab.com/c2platform/ansible-collection-dev/badges/master/pipeline.svg?style=flat-square&key_text=Pipeline+Status&key_width=90)](https://gitlab.com/c2platform/ansible-collection-dev/-/pipelines)
[![Latest Release](https://gitlab.com/c2platform/ansible-collection-dev/-/badges/release.svg?style=flat-square)](https://gitlab.com/c2platform/ansible-collection-dev/-/pipelines)
[![Ansible Galaxy](https://img.shields.io/badge/Galaxy-_c2platform.dev-blue.svg)](https://galaxy.ansible.com/ui/repo/published/c2platform/dev/)

See full [README](https://gitlab.com/c2platform/ansible-collection-dev/-/blob/master/README.md).
